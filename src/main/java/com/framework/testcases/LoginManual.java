package com.framework.testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LoginManual {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		driver.get("https://acme-test.uipath.com/account/login");
		driver.findElementById("email").sendKeys("mohanmanoj.amara@gmail.com");
		driver.findElementById("password").sendKeys("Manoj1980");
		driver.findElementById("buttonLogin").click();
		WebElement selVendor = driver.findElementByXPath("(//button[@class = 'btn btn-default btn-lg'])[4]");
		WebElement selSearForVendor = driver.findElementByXPath("//a[contains(text() ,'Search for Vendor') ]");
		Actions builder = new Actions(driver);
		builder.moveToElement(selVendor).pause(2000);
		builder.click(selSearForVendor).perform();
		driver.findElementById("vendorTaxID").sendKeys("RO254678");
		driver.findElementById("buttonSearch").click();
		
		
		
	}

}
