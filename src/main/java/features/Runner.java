package features;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith (Cucumber.class)
@CucumberOptions(features = "src/main/java/features/",
				glue = "features",
				dryRun = false,
				snippets=SnippetType.CAMELCASE,
				plugin = {"pretty","html:Reports/CucumberBasicReport"})
				
public class Runner {
	
	
	

}
