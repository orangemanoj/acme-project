package features;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class Steps {
	
	public ChromeDriver driver;
	
	@Given("open chrome browser")
	public void openChromeBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Given("maximise the browser")
	public void maximiseTheBrowser() {
		driver.manage().window().maximize();
	}

	@Given("set thetimeout")
	public void setThetimeout() {
		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
	}

	@Given("Enter the Url")
	public void enterTheUrl() {
		driver.get("https://acme-test.uipath.com/account/login");
	}

	@Given("Enter the Username")
	public void enterTheUsername() {
		driver.findElementById("email").sendKeys("mohanmanoj.amara@gmail.com");
	}

	@Given("Enter the Password")
	public void enterThePassword() {
		driver.findElementById("password").sendKeys("Manoj1980");
	}

	@When("click the Login button")
	public void clickTheLoginButton() {
		driver.findElementById("buttonLogin").click();
	}

	@When("mouseover Vendors")
	public void mouseoverVendors() {
		WebElement selVendor = driver.findElementByXPath("(//button[@class = 'btn btn-default btn-lg'])[4]");
	}

	@When("click Search for Vendors")
	public void clickSearchForVendors() {
		WebElement selVendor = driver.findElementByXPath("(//button[@class = 'btn btn-default btn-lg'])[4]");
		WebElement selSearForVendor = driver.findElementByXPath("//a[contains(text() ,'Search for Vendor') ]");
		Actions builder = new Actions(driver);
		builder.moveToElement(selVendor).pause(2000);
		builder.click(selSearForVendor).perform();
	}

	@When("Enter Vendor TxnID")
	public void enterVendorTxnID() {
		driver.findElementById("vendorTaxID").sendKeys("RO254678");
	}

	@When("click Search")
	public void clickSearch() {
		driver.findElementById("buttonSearch").click();
	}


}
